import java.util.List;

public class Movie {
    public String title;
    public String year;
    public String filmGenre;
    public Director director;
    public List<Actor> actors;

    public void print() {
        System.out.println("Title: " + title);
        System.out.println("Year: " + year);
        System.out.println("Genre: " + filmGenre);
        System.out.println("Director details:");
        director.print();

        for (int i = 0; i < actors.size(); i++) {
            System.out.println("Details of actor nr: " + i);
            actors.get(i).print();
        }
    }
}
