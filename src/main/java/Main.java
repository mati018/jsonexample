import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Main {

    public static void main(String[] args) {
        Gson gson = new Gson();
        String json = "{ \"library\":[ { \"title\":\"Dom zly\", \"year\":\"2009\", \"filmGenre\":\"drama\", \"director\":{ \"firstName\":\"Wojciech\", \"lastName\":\"Smarzowski\" }, \"actors\":[ { \"firstName\":\"Arkadiusz\", \"lastName\":\"Jakubik\" }, { \"firstName\":\"Marian\", \"lastName\":\"Dzięgiel\" }, { \"firstName\":\"Kinga\", \"lastName\":\"Preis\" } ] }, { \"title\":\"Nietykalni\", \"year\":\"2011\", \"filmGenre\":\"comedy\", \"director\":{ \"first_name\":\"Olivier\", \"lastName\":\"Nakache\" }, \"actors\":[ { \"firstName\":\"François\", \"lastName\":\"Cluzet\" }, { \"firstName\":\"Omar\", \"lastName\":\"Sy\" } ] } ] }";
        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(json).getAsJsonObject();
        Library library = gson.fromJson(obj, Library.class);
        library.print();
    }
}