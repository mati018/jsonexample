import java.util.List;

public class Library {
    public List<Movie> library;

    public void print() {
        System.out.println("Number of films: " + library.size());

        for (Movie movie : library) {
            System.out.println();
            movie.print();
        }
    }
}
